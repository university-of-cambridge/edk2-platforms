## @file
#  Configuration Manager Dxe
#
#  Copyright (c) 2020, ARM Limited. All rights reserved.
#
#  SPDX-License-Identifier: BSD-2-Clause-Patent
##

[Defines]
  INF_VERSION                    = 0x0001001B
  BASE_NAME                      = ConfigurationManagerDxe
  FILE_GUID                      = 6F9C3B47-6F7D-44B6-87E5-4B7F44A60147
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0
  ENTRY_POINT                    = ConfigurationManagerDxeInitialize

#
# The following information is for reference only and not required by the build tools.
#
#  VALID_ARCHITECTURES           = AARCH64
#

[Sources]
  ConfigurationManagerFvp.c
  ConfigurationManager.c

[Packages]
  ArmPkg/ArmPkg.dec
  ArmPlatformPkg/ArmPlatformPkg.dec
  DynamicTablesPkg/DynamicTablesPkg.dec
  MdeModulePkg/MdeModulePkg.dec
  MdePkg/MdePkg.dec
  Platform/ARM/Morello/MorelloPlatform.dec

[LibraryClasses]
  PlatformAslTablesLib
  UefiDriverEntryPoint
  UefiRuntimeServicesTableLib

[Protocols]
  gEdkiiConfigurationManagerProtocolGuid

[FixedPcd]
  ## PL011 Serial Debug UART
  gArmPlatformTokenSpaceGuid.PcdSerialDbgRegisterBase
  gArmPlatformTokenSpaceGuid.PcdSerialDbgUartBaudRate
  gArmPlatformTokenSpaceGuid.PcdSerialDbgUartClkInHz

  gArmPlatformTokenSpaceGuid.PL011UartClkInHz
  gArmPlatformTokenSpaceGuid.PL011UartInterrupt

  gArmTokenSpaceGuid.PcdArmArchTimerHypIntrNum
  gArmTokenSpaceGuid.PcdArmArchTimerIntrNum
  gArmTokenSpaceGuid.PcdArmArchTimerSecIntrNum
  gArmTokenSpaceGuid.PcdArmArchTimerVirtIntrNum

  # SBSA Generic Watchdog
  gArmTokenSpaceGuid.PcdGenericWatchdogControlBase
  gArmTokenSpaceGuid.PcdGenericWatchdogEl2IntrNum
  gArmTokenSpaceGuid.PcdGenericWatchdogRefreshBase

  gArmTokenSpaceGuid.PcdGicDistributorBase
  gArmTokenSpaceGuid.PcdGicInterruptInterfaceBase
  gArmTokenSpaceGuid.PcdGicRedistributorsBase

  gEfiMdeModulePkgTokenSpaceGuid.PcdSerialRegisterBase
  gEfiMdePkgTokenSpaceGuid.PcdUartDefaultBaudRate
[Pcd]

[Depex]
  TRUE
