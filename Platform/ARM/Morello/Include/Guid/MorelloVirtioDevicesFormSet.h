/** @file
*
*  Copyright (c) 2020, ARM Limited. All rights reserved.
*
*  SPDX-License-Identifier: BSD-2-Clause-Patent
*
**/

#ifndef  MORELLO_VIRTIO_DEVICES_FORMSET_H__
#define  MORELLO_VIRTIO_DEVICES_FORMSET_H__

#define MORELLO_VIRTIO_BLOCK_GUID  \
  { 0x2b6e62d0, 0x9346, 0x4e1a, { 0xaa, 0x1e, 0xcb, 0x01, 0xc3, 0x23, 0x4a, 0x00 } }

#endif
